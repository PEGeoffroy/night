![logo_night](./images/logo_night.png)



# Projet CPro6 : NIGHT

> Pierre-Élie Geoffroy

##  Résumé

> NIGHT (pour *NIGHT Is a Gamemaster Heroic Toolbox* #acronyme récursif) a pour ambition de devenir une boite à outil multi-plateforme pour les meneurs de jeux.

### Contexte

Le jeu de rôle est un jeu composé d'un meneur de jeu et de joueurs. Le meneur qui à un rôle de narrateur va tisser une histoire à laquelle se mêle les interactions des joueurs. NIGHT se veut être un outil pour assister le meneur dans sa narration.

### Objectif

Pour la V1 il va s'agir de pouvoir stocker des informations sur les personnages qu'ils soient joueur ou non pour ensuite les manipuler (CRUD). L'objet de la seconde partie de cette V1 concerne la génération procédurale d'un personnage.

##  Spécificité technique

* Symfony (API) pour le back, avec une BDD en mariadb
* React pour l'appli front
* Docker pour l'environnement de dev